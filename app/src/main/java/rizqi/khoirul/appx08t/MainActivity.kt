package rizqi.khoirul.appx08t

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val RC_SUKSES : Int = 100
    var bgH : String = "RED"
    var fHead : Int = 24
    var fTitle : Int = 20
    var bg : String = "WHITE"
    var isi : String = "121_137"
    lateinit var pref : SharedPreferences
    val PREF_NAME = "Setting"
    val FIELD_FONT_HEADER = "Font_Size_Header"
    val FIELD_FONT_TITLE = "Font_Size_Title"
    val FIELD_BACK = "BACKGROUND"
    val FIELD_BACKHEAD = "BACKGROUND_HEADER"
    val FIELD_TEXT = "text"
    val DEF_FONT_HEADER = fHead
    val DEF_FONT_TITLE = fTitle
    val DEF_BACK = bg
    val DEF_BACKHEAD = bgH
    val DEF_ISI = isi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txIsi.setText(pref.getString(FIELD_TEXT,DEF_ISI))
        fHead = pref.getInt(FIELD_FONT_HEADER,DEF_FONT_HEADER)
        fTitle = pref.getInt(FIELD_FONT_TITLE,DEF_FONT_TITLE)
        bg = pref.getString(FIELD_BACK,DEF_BACK).toString()
        bgH = pref.getString(FIELD_BACKHEAD,DEF_BACKHEAD).toString()
        pemanggil()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_main,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSet->{
                var intent = Intent(this,SettingActivity::class.java)
                intent.putExtra("x",txIsi.text.toString())
                startActivityForResult(intent,RC_SUKSES)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == RC_SUKSES) {

                txIsi.setText(data?.extras?.getString("isidetail"))
                isi = txIsi.text.toString()
                bg = data?.extras?.getString("bgHcolor").toString()
                bgH = data?.extras?.getString("bgcolor").toString()
                fHead = data?.extras?.getInt("tHead").toString().toInt()
                fTitle = data?.extras?.getInt("tTitle").toString().toInt()
                simpan()
                pemanggil()
            }
        }
    }

    fun simpan(){
        pref = getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
        val prefEdit = pref.edit()
        prefEdit.putInt(FIELD_FONT_HEADER,fHead)
        prefEdit.putInt(FIELD_FONT_TITLE,fTitle)
        prefEdit.putString(FIELD_TEXT,isi)
        prefEdit.putString(FIELD_BACK,bg)
        prefEdit.putString(FIELD_BACKHEAD,bgH )
        prefEdit.commit()
    }

    fun pemanggil(){
        Font_Header()
        Font_Title()
        background()
        backgroundHeader()
    }

    fun background(){
        if(bgH=="Blue"){
            txHeader.setBackgroundColor(Color.BLUE)
        }
        else if(bgH=="Yellow"){
            txHeader.setBackgroundColor(Color.YELLOW)
        }
        else if(bgH=="Green"){
            txHeader.setBackgroundColor(Color.GREEN)
        }
        else if(bgH=="Black"){
            txHeader.setBackgroundColor(Color.BLACK)
        }
        else if(bgH=="White"){
            txHeader.setBackgroundColor(Color.WHITE)
        }
        else{
            txHeader.setBackgroundColor(Color.RED)
        }
    }

    fun backgroundHeader(){
        if(bg=="BLUE"){
            constraintLayout.setBackgroundColor(Color.BLUE)
        }
        else if(bg=="YELLOW"){
            constraintLayout.setBackgroundColor(Color.YELLOW)
        }
        else if(bg=="GREEN"){
            constraintLayout.setBackgroundColor(Color.GREEN)
        }
        else if(bg=="BLACK"){
            constraintLayout.setBackgroundColor(Color.BLACK)
        }
    }

    fun Font_Header(){
        txHeader.textSize = fHead.toFloat()
    }

    fun Font_Title(){
        txTitle.textSize = fTitle.toFloat()
    }
}
